# from cassandra import cqlengine
from kafka import KafkaProducer, KafkaConsumer
import json


def main():
    producer = KafkaProducer(bootstrap_servers='localhost:9092')
    for _ in range(10):
        producer.send('test', b'my_message')
    producer.flush()


if __name__ == '__main__':
    main()
