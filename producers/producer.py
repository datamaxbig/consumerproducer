import threading
from kafka import KafkaProducer
from cassandra.cluster import Cluster
import time, random


class CarVelocityGenerator(threading.Thread):
    cars = None
    velocities = []
    producer = KafkaProducer(bootstrap_servers='localhost:9092')

    def __init__(self):
        cluster = Cluster()
        session = cluster.connect()
        self.cars = session.execute('SELECT * FROM vehiclelawsystem.autos')
        for i in range(500):
            if i < 450:
                self.velocities.append(random.randint(40, 80))
            else:
                self.velocities.append(random.randint(81, 150))


    def run(self) -> None:
        while True:
            n = random.randint(0, 7000)
            car = {
                'id' : str(self.cars[n][0]),
                'anio' : self.cars[n][1],
                'marca' : self.cars[n][2],
                'modelo' : self.cars[n][3],
                'placa' : self.cars[n][4],
                'velocidad' : self.velocities[random.randint(0, 499)]
            }
            self.producer.send('test', bytes(str(car), 'utf-8'))
            self.producer.flush()
            time.sleep(1000.0 / 1000.0)


carvel = CarVelocityGenerator()
carvel.run()
