from kafka import KafkaConsumer
import threading
from cassandra.cluster import Cluster
import json, uuid, datetime

class Car(object):
    def __init__(self, data):
        self.__dict__ = json.loads(data)

class InfractionGenerator(threading.Thread):
        cars = None

        @staticmethod
        def infraccionar(car):
            cluster = Cluster()
            session = cluster.connect()
            session.execute(
                """
                INSERT INTO vehiclelawsystem.infracciones (id, fecha, hora, infraccion, infractor_id, sancion)
                VALUES (%s, %s, %s, %s, %s, %s)
                """,
                (uuid.uuid4(), datetime.datetime.now().date(), datetime.datetime.now().time(), 'Exceso de velocidad', uuid.UUID(car['id']), car['velocidad']*100)
            )

        def run(self) -> None:
            consumer = KafkaConsumer('test', auto_offset_reset='earliest', bootstrap_servers='localhost:9092')
            for msg in consumer:
                str_car = ''.join(map(chr, msg.value)).replace("\'", "\"")
                car = json.loads(str_car)
                if car['velocidad'] > 80:
                    self.infraccionar(car)
            consumer.close()

carinf = InfractionGenerator()
carinf.run()
