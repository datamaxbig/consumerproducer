import uuid

import MySQLdb
from cassandra.cluster import Cluster

import random


def genPlaca():
    ALPHABETH = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    NUMBERS = '0123456789'
    out = ''
    for i in range(3):
        if (random.randint(0, 1) == 0):
            out += ALPHABETH[random.randint(0, 25)]
        else:
            out += NUMBERS[random.randint(0, 9)]
    out += '-'
    for i in range(4):
        if (random.randint(0, 1) == 0):
            out += ALPHABETH[random.randint(0, 25)]
        else:
            out += NUMBERS[random.randint(0, 9)]
    return out


db = MySQLdb.connect(host='127.0.0.1', user='root', passwd='root123', db='automobiles')
cursor = db.cursor()
cursor.execute("SELECT * FROM VehicleModelYear ORDER BY RAND()")
rows = cursor.fetchall()

cluster = Cluster()
session = cluster.connect()
# local_query = 'SELECT * FROM vehiclelawsystem.autos'
# rows = session.execute(local_query)
# for row in rows:
#     print(row)

for row in rows:
    placa = genPlaca()
    id = uuid.uuid4()
    session.execute(
        """
        INSERT INTO vehiclelawsystem.autos (id, anio, marca, modelo, placa)
        VALUES (%s, %s, %s, %s, %s)
        """,
        (id, row[1], row[2], row[3], placa)
    )
